var gulp = require('gulp');
var browser_sync = require('browser-sync');
var connect = require('gulp-connect-php');
var sass = require('gulp-sass');
var del = require('del');

var paths = {
    index: ['*.php', '*.ini'],
    php_theme: 'theme/**/*',
    files: 'files/*.*',
    sass: 'css/*.scss',
    css: ['css/*.css', 'css/languages.png'],
    scripts: ['js/bootstrap.min.js', 'js/app.js'],
    images: 'images/*.*'
};

gulp.task('default',
    [
        'files',
        'css',
        'sass',
        'scripts',
        'images',
        'index',
        'watch',
        'connect-sync'
    ]);

gulp.task('clean', function() {
    del(['build/**']);

});

gulp.task('files', function() {
    gulp.src(paths.files)
        .pipe(gulp.dest('build/files'));
});

gulp.task('scripts', function() {
    gulp.src(paths.scripts)
        .pipe(gulp.dest('build/js'));
});

gulp.task('css', function () {
    gulp.src(paths.css)
        .pipe(gulp.dest('build/css'));
});

gulp.task('sass', function () {
    gulp.src(paths.sass)
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('build/css'));
});

gulp.task('images', ['clean'], function() {
    gulp.src(paths.images)
        .pipe(gulp.dest('build/images'));
});

gulp.task('index', function() {
    gulp.src(paths.index)
        .pipe(gulp.dest('build'));
    gulp.src(paths.php_theme)
        .pipe(gulp.dest('build/theme'));
});

gulp.task('watch', function() {
    gulp.watch(paths.css, ['css']);
    gulp.watch(paths.sass, ['sass']);
    gulp.watch(paths.scripts, ['scripts']);
    gulp.watch(paths.images, ['images']);
    gulp.watch(paths.index, ['index']);
    gulp.watch(paths.php_theme, ['index']);
});

gulp.task('connect-sync', function() {
    connect.server({
        base: './build/'
    }, function (){
        browser_sync({
            proxy: '127.0.0.1:8000'
        });
    });

    gulp.watch('**/*.php').on('change', function () {
        browser_sync.reload();
    });
});
