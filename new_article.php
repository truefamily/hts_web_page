<?php
include('theme/head.php');
include('theme/menu.php');
require('Connection.php');
$link = Connection::getDb();
?>

<div class="container main_content">
    <?php if(!isset($_SESSION['username'])) { ?>
        <p><b>Лучше сначала пройти аутентификацию.</b></p>
    <?php exit; } ?>
    <p><b>Добавить новость</b></p>
    <form action="save_new_article.php" method="post">
        <label for="russian_title">Заголовок</label>
        <div class="form-group">
            <textarea id="russian_title" class="form-control" rows="1" name="title" placeholder="На русском" required></textarea>
        </div>
        <div class="form-group">
            <textarea id="english_title" class="form-control" rows="1" name="title_en" placeholder="На английском" required></textarea>
        </div>
        <label for="russian_text">Текст статьи</label>
        <div class="form-group">
            <textarea id="russian_text" class="form-control" rows="7" name="content" placeholder="На русском" required></textarea>
        </div>
        <div class="form-group">
            <textarea id="english_text" class="form-control" rows="7" name="content_en" placeholder="На английском" required></textarea>
        </div>
        <div class="form-group">
            <input type="password" class="form-control" name="password" placeholder="Пароль" required>
        </div>
        <div class="form-group">
            <input type="text" class="form-control" name="version" placeholder="Новая версия? (Текущая <?= Connection::getVersion() ?>)">
        </div>
        <div class="admin-margin">
            <button type="submit" class="save btn btn-lg btn-primary">Сохранить</button>
            <button type="button" class="btn btn-lg" onclick="location.href='/index.php';">Отмена</button>
        </div>
    </form>
</div>

<script src="/js/app.js"></script>
</body>
</html>