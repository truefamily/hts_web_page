<?php
include('theme/head.php');
include('theme/menu.php');
?>

<div class="container main_content">
    <p><b><?= $t['site.instruction.title'] ?></b></p>
    <p><?= $t['site.instruction.1'] ?></p>
    <img src="<?= $t['site.instruction.image_1'] ?>" />
    <p><?= $t['site.instruction.2'] ?></p>
    <img src="<?= $t['site.instruction.image_2'] ?>" />
    <p><?= $t['site.instruction.2_5'] ?></p>
    <img src="<?= $t['site.instruction.image_3'] ?>" />
    <p><?= $t['site.instruction.3'] ?></p>
    <img src="<?= $t['site.instruction.image_4'] ?>" />
    <p><?= $t['site.instruction.4'] ?></p>
    <img src="<?= $t['site.instruction.image_5'] ?>" />
</div>

<?php include('theme/footer.php'); ?>
<script src="/js/app.js"></script>
</body>
</html>