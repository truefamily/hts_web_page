<?php
include('theme/head.php');
include('theme/menu.php');
require 'Connection.php';
$conf = Connection::getConfig();
$link = Connection::getDb();
 ?>
<div class="container main_content">
    <?php
    $result = mysqli_query($link, 'SELECT password FROM users WHERE id = 1');
    $hash = null;
    while ($row = $result->fetch_assoc()) {
        $hash = $row['password'];
    }

    $pass = $_POST["password"];

    if (password_verify($pass, $hash)) {
        echo 'Пароль норм, идем дальше!<br/>';
    } else {
        echo 'Опять забыл?<br/>';
        exit();
    }

    $title = $_POST["title"];
    $title_en = $_POST["title_en"];
    $content = nl2br($_POST["content"]);
    $content_en = nl2br($_POST["content_en"]);
    $date = date("Y-m-d H:i:s");

    if (mysqli_query($link, "INSERT INTO articles (title, title_en, article_date, content, content_en) 
        VALUES('$title', '$title_en', '$date', '$content', '$content_en')") === TRUE) {
        echo "Статья добавлена.<br/>";
    } else {
        die(mysqli_error($link));
    }

    $version = $_POST["version"];
    if ($version != null && $version != "") {
        if (mysqli_query($link, "UPDATE server_version SET version='$version'") === TRUE) {
            echo "Версия изменена.<br/>";
        } else {
            die(mysqli_error($link));
        }
    }

    mysqli_close($link);
    ?>
</div>
</body>
</html>