/**
 * Created by root on 13.05.2016.
 */
$(window).scroll(function() {
    if ($(this).scrollTop() > 1){
        $('header').addClass("sticky");
        alert("YO");
    }
    else{
        $('header').removeClass("sticky");
    }
});

function setCookie (name, value, expires, path, domain, secure) {
    document.cookie = name + "=" + escape(value) +
        ((expires) ? "; expires=" + expires : "") +
        ((path) ? "; path=" + path : "") +
        ((domain) ? "; domain=" + domain : "") +
        ((secure) ? "; secure" : "");
}

function setEngLang() {
    setCookie("lang", "en", "Mon, 01-Jan-2020 00:00:00 GMT", "/");
    location.reload();
}

function setRuLang() {
    setCookie("lang", "ru", "Mon, 01-Jan-2020 00:00:00 GMT", "/");
    location.reload();
}