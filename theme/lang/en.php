<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 05.08.2016
 * Time: 21:48
 */

// Меню сайта
$t['site.menu.index'] = 'Main';
$t['site.menu.blog'] = 'Blog';
$t['site.menu.instruction'] = 'Instruction';
$t['site.menu.policy'] = 'Privacy policy';
$t['site.menu.policy_pro'] = 'Policy for PRO';
$t['site.menu.contacts'] = 'Contacts';
$t['site.menu.login'] = 'Login';
$t['site.menu.logout'] = 'Logout';
$t['site.menu.new_article'] = 'New article';

// Главная страница
$t['site.main.main_text'] = '<b>HomeTheatre</b> is an application for remote control your home theater. 
Using your Android smartphone you will be able to control the watching from your sofa.
For operating mobile application <b>Home Theater Server</b> is required. 
Download it from the link below. There is ReadMe.txt in the archive, read it for a successful server setup.';
$t['site.main.download'] = 'Download the server (for Windows), ver. ';
$t['site.main.download_from_google'] = 'If you don\'t have <b>Home Theater Remote</b>, download it from';
$t['site.main.pro_version_text'] = 'There is a Pro version of application with more functions. If you like this app but want more - try the Pro version on Google Play:<br/>';
$t['site.main.no_news'] = 'There is no news. But soon will be!';

// Блог
$t['site.blog.last_news'] = 'Latest news';

// Инструкция
$t['site.instruction.title'] = 'Server setting up instruction';
$t['site.instruction.1'] = '1. At first you need to configure <b>HTS_GUI.exe</b>.';
$t['site.instruction.2'] = '2. Then in the settings of  <b>Media Player Classic - Home Cinema</b> 
    (View->Options...->Web Interface) check the box <b>"listen on port"</b> and write <b>the port "13579"</b>. 
    Check the box <b>"Allow access only from localhost only"</b> if it is unckecked (for your safety).';
$t['site.instruction.2_5'] = 'The program works correctly only for two languages: Russian and English.';
$t['site.instruction.3'] = '3. Then run the <b>HTS.exe</b>. In the console you should see the message about successful server startup at the address
        you specified in the server settings.';
$t['site.instruction.4'] = '4. Then you can specify the connection settings in the mobile app.';
$t['site.instruction.image_1'] = '/images/settings_en.png';
$t['site.instruction.image_2'] = '/images/mpc_settings_en.png';
$t['site.instruction.image_3'] = '/images/language_en.png';
$t['site.instruction.image_4'] = '/images/ht_server_en.png';
$t['site.instruction.image_5'] = '/images/mobile_en.png';

$t['site.policy.title'] = 'PRIVACY POLICY';

// Контакты
$t['site.contacts.about'] = 'About us';
$t['site.contacts.developed_by'] = 'The website has developed by';
$t['site.contacts.contact_us'] = 'Contact us';
$t['site.contacts.silakov'] = 'Igor Silakov';
$t['site.contacts.silakova'] = 'Daria Silakova';

// =========== Шаблоны ===========
$t['site.head.title'] = 'Home Theatre Server - the remote control of your home theater. Mobile application for Android.';

$t['site.short_article.title'] = 'title_en';
$t['site.short_article.content'] = 'content_en';

$t['site.footer.text'] = 'Igor Silakov';