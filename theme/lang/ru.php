<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 05.08.2016
 * Time: 21:48
 */

// Меню сайта
$t['site.menu.index'] = 'Главная';
$t['site.menu.blog'] = 'Блог';
$t['site.menu.instruction'] = 'Инструкция';
$t['site.menu.policy'] = 'Политика конфиденциальности';
$t['site.menu.policy_pro'] = 'Политика конфиденциальности для PRO-версии';
$t['site.menu.contacts'] = 'Контакты';
$t['site.menu.login'] = 'Войти';
$t['site.menu.logout'] = 'Выйти';
$t['site.menu.new_article'] = 'Новая статья';

// Главная страница
$t['site.main.main_text'] = '<b>HomeTheatre</b> - приложение для удаленного управления домашним кинотеатром. 
С помощью вашего Android-смартфона вы сможете управлять просмотром не вставая с дивана!
Для работы мобильного приложения необходим сервер <b>Home Theater Server</b>. 
Скачайте его по ссылке ниже. В архиве лежит ReadMe.txt, прочитайте его для успешной настройки сервера.';
$t['site.main.download'] = 'Загрузить сервер (для Windows), версия ';
$t['site.main.download_from_google'] = 'Если вы еще не скачали <b>Home Theater Remote</b>, сделайте это на';
$t['site.main.pro_version_text'] = 'Существует PRO версия приложения, без рекламы и с полной функциональностью, 
    почитать о всех преимуществах покупки PRO версии вы можете на странице приложения в Google Play:<br/>';
$t['site.main.no_news'] = 'Новостей нет. Но скоро будут!';

// Блог
$t['site.blog.last_news'] = 'Последние новости';

// Инструкция
$t['site.instruction.title'] = 'Инструкция по настройке сервера';
$t['site.instruction.1'] = '1. Сначала необходимо настроить <b>HTS_GUI.exe</b>.';
$t['site.instruction.2'] = '2. Затем в настройках <b>Media Player Classic - Home Cinema</b> (Вид->Настройки->WEB-интерфейс)
    нужно поставить галочку <b>"Слушать порт:"</b> и прописать <b>порт "13579"</b>, а также установить (если не стоит) 
    галочку <b>"Разрешить доступ только с локального компьютера"</b> (для вашей безопасности).';
$t['site.instruction.2_5'] = 'Программа корректно работает только для двух языков: русского и английского.';
$t['site.instruction.3'] = '3. Далее необходимо запустить <b>HTS.exe</b>, в консоли должно появиться сообщение об успешном 
    запуске сервера по адресу, который вы указали в настройках.';
$t['site.instruction.4'] = '4. Затем можно прописать настройки соединения в мобильном приложении.';
$t['site.instruction.image_1'] = '/images/settings.png';
$t['site.instruction.image_2'] = '/images/mpc_settings.png';
$t['site.instruction.image_3'] = '/images/language.png';
$t['site.instruction.image_4'] = '/images/ht_server.png';
$t['site.instruction.image_5'] = '/images/mobile.png';

$t['site.policy.title'] = 'ПОЛИТИКА КОНФИДЕНЦИАЛЬНОСТИ';

// Контакты
$t['site.contacts.about'] = 'О нас';
$t['site.contacts.developed_by'] = 'Сайт разработали';
$t['site.contacts.contact_us'] = 'Связаться с нами';
$t['site.contacts.silakov'] = 'Силаков Игорь';
$t['site.contacts.silakova'] = 'Силакова Дарья';

// =========== Шаблоны ===========
$t['site.head.title'] = 'Home Theatre Server - удаленное управление домашним кинотеатром. Мобильное приложение на Android.';

$t['site.short_article.title'] = 'title';
$t['site.short_article.content'] = 'content';

$t['site.footer.text'] = 'Игорь Силаков';