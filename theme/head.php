<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<?php
$lang = $_COOKIE["lang"];
if ($lang == null) { $lang = "en"; setcookie("lang", $lang); }
if($lang == "ru") { include("lang/ru.php"); }
if($lang == "en") { include("lang/en.php"); }
date_default_timezone_set('Europe/Moscow');
?>

<head>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/languages.min.css">
    <link rel="stylesheet" href="/css/app.css">
    <!-- <link rel="stylesheet" href="/css/hover.css"> -->
    <link rel="shortcut icon" href="/images/favicon.gif">
    <meta charset="UTF-8">
    <meta name="author" content="Игорь Силаков">
    <meta name="copyright" content="Все права принадлежат Игорю Силакову">
    <meta http-equiv="content-language" content="ru">
    <meta name=viewport content="width=device-width, initial-scale=1">
    <meta name="keywords" content="home, theater, remote, удаленное, управление, домашний, кинотеатр, мобильное, приложение">
    <title><?= $t['site.head.title'] ?></title>
</head>
<body>