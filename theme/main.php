<div class="container main_content">
    <p><b>HOME THEATER SERVER</b></p>
    <p style="text-align: left"><?= $t['site.main.main_text']; ?></p>
    <a href="/files/HomeTheaterServer.zip">
        <button type="button" class="dwnld btn btn-lg btn-primary"><?= $t['site.main.download'] . Connection::getVersion(); ?></button>
    </a>
    <p>
        <?= $t['site.main.download_from_google']; ?>
        <a href="https://play.google.com/store/apps/details?id=com.trueberry.hometheatrelite" target="_blank">Google Play.</a>
    <p>
        <?= $t['site.main.pro_version_text']; ?>
        <a href="https://play.google.com/store/apps/details?id=com.trueberry.hometheatre" target="_blank"><img src="/images/google_play.png" /></a>
    <?php
    if ($articles):
        foreach ($articles as $article):
            include("short_article.php");
         endforeach;
    else:
        echo $t['site.main.no_news'];
    endif;
    ?>
</div>