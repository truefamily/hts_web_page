<div class="article_main">
    <div class="article_date"><h4><?=date_create($article['article_date'])->format("d.m.Y")?></h4></div>
    <div class="article_title"><h3><?=$article[$t['site.short_article.title']]?></h3></div>
    <div class="article_text"><?=$article[$t['site.short_article.content']]?></div>
</div>