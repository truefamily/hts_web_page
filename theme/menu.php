<?php
session_start();
?>
<header>
    <nav class="navbar navbar-fixed-top">
        <div class="container">
            <a class="navbar-brand" href="/index.php"><img src="/images/logo.png" width="32px"/> </a>
            <div id="navbar" class="navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active hvr-underline-from-center"><a href="/index.php" class=""><?=$t['site.menu.index']?></a></li>
                    <li class="hvr-underline-from-center"><a href="/blog.php"><?=$t['site.menu.blog']?></a></li>
                    <li class="hvr-underline-from-center"><a href="/instruction.php"><?=$t['site.menu.instruction']?></a></li>
                    <li class="hvr-underline-from-center"><a href="/policy.php"><?=$t['site.menu.policy']?></a></li>
                    <li class="hvr-underline-from-center"><a href="/policy_pro.php"><?=$t['site.menu.policy_pro']?></a></li>
                </ul>
                <ul class="nav navbar-nav pull-right">
                    <?php if(!isset($_SESSION['username'])) { ?>
                        <li class="hvr-underline-from-center"><a href="/login.php"><?=$t['site.menu.login']?></a></li>
                    <?php } else { ?>
                        <li class="hvr-underline-from-center"><a href="/logout.php"><?=$t['site.menu.logout']." (".$_SESSION['username'].")"?></a></li>
                        <li class="hvr-underline-from-center"><a href="/new_article.php"><?=$t['site.menu.new_article']?></a></li>
                    <?php } ?>

                    <li class="langs">
                        <a class="hvr-underline-from-center" onclick="setEngLang()"><span class="lang-lg" lang="en"></span></a>
                    </li>
                    <li class="langs">
                        <a class="hvr-underline-from-center" onclick="setRuLang()"><span class="lang-lg" lang="ru"></span></a>
                    </li>
                    <li class="hvr-underline-from-center"><a href="/about.php"><?=$t['site.menu.contacts']?></a></li>
                </ul>
            </div>
        </div>
    </nav>
</header>