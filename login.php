<?php
include('theme/head.php');
include('theme/menu.php');
require 'Connection.php';
$link = Connection::getDb();
?>
<div class="container main_content">
<?php
 if ($_SERVER['REQUEST_METHOD'] == 'GET') {
     if (isset($_SESSION['username'])) {
         ?>
         <p><b>Вы уже авторизованы, <?= $_SESSION['username'] ?>!</b></p>
         <?php
         exit;
     } ?>
     <p><b>Залогиниться на сайте</b></p>
     <form action="login.php" method="post">
         <label for="username">Логин</label>
         <div class="form-group">
             <input type="text" id="username" class="form-control" name="username" required />
         </div>
         <label for="password">Пароль:</label>
         <div class="form-group">
             <input type="password" id="password" class="form-control" name="password" required />
         </div>
         <div>
             <button type="submit" class="save btn btn-lg btn-primary">Войти</button>
             <button type="button" class="btn btn-lg" onclick="location.href='/index.php';">Отмена</button>
         </div>
     </form>
<?php } else {
     $username = $_POST["username"];
     $result = mysqli_query($link, "SELECT * FROM users WHERE username='".$username."' LIMIT 1");
     $hash = "";
     while($row = mysqli_fetch_array($result)) {
         $hash = $row['password'];
     }
     if ($hash != "" && password_verify($_POST['password'], $hash)) {
         session_start();
         $_SESSION['username']=$username;
         header("Location: /index.php");
     } else {
         $text = "Неправильный логин или пароль!";
     }

} ?>
    <?php if(isset($text)) { ?>
        <p><b><?= $text ?></b></p>
    <?php } ?>
</div>
<script src="/js/app.js"></script>
</body>
</html>