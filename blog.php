<?php
include('theme/head.php');
include('theme/menu.php');
require 'Connection.php';
$conf = Connection::getConfig();
$link = Connection::getDb();

$articles = mysqli_query($link, 'SELECT * FROM articles ORDER BY article_date DESC LIMIT '. $conf['pp']);
mysqli_close($link);
// грузим меню
?>
<div class="container main_content">
    <p><b><?= $t['site.blog.last_news'] ?></b></p>
    <?php
    if ($articles):
        foreach ($articles as $article):
            include("theme/short_article.php");
        endforeach;
    else:
       echo '<p>' . $t['site.main.no_news'] . '</p>';
    endif; ?>
</div>

<?php
// грузим футер
include('theme/footer.php');
?>
<script src="/js/app.js"></script>
</body>
</html>