<?php
include('theme/head.php');
include('theme/menu.php');
require 'Connection.php';
$conf = Connection::getConfig();
$link = Connection::getDb();

$articles = mysqli_query($link, 'SELECT * FROM articles ORDER BY article_date DESC, id DESC LIMIT '. $conf['mainpp']);
mysqli_close($link);

include('theme/main.php');
include('theme/footer.php');
?>
<script src="js/app.js"></script>
</body>
</html>