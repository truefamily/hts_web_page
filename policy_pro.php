<?php
include('theme/head.php');
include('theme/menu.php');
?>

<div class="container main_content" style="text-align: left;">
    <p><b><?= $t['site.policy.title'] ?></b></p>
    <p>This privacy policy governs your use of the software application Home Theater Remote PRO (“Application”) for mobile devices that was created by Igor Silakov. Home Theater Remote PRO is an app for remote control of your home theater. It is all-purpose and operate with TVs, video projectors and audio devices of different companies.
    <p><b>User Provided Information</b>
    <p>The Application obtains the information you provide when you download and register the Application. Registration with us is optional. However, please keep in mind that you may not be able to use some of the features offered by the Application unless you register with us.
    <p>We may also use the information you provided us to contact your from time to time to provide you with important information, required notices and marketing promotions.
    <p><b>Automatically Collected Information</b>
    <p>In addition, the Application may collect certain information automatically, including, but not limited to, the type of mobile device you use, your mobile devices unique device ID, the IP address of your mobile device, your mobile operating system, the type of mobile Internet browsers you use, and information about the way you use the Application.
    <p><b>Does the Application collect precise real time location information of the device?</b>
    <p>This Application does not collect precise information about the location of your mobile device.
    <p><b>Do third parties see and/or have access to information obtained by the Application?</b>
    <p>No.
    <p><b>Automatic Data Collection and Advertising</b>
    <p>We analyze only the fact of the call, without collecting information about the call. This is necessary in order to send a command to pause the movie server. This information is not stored anywhere.</p>
    <p><b>Children</b>
    <p>We do not use the Application to knowingly solicit data from or market to children under the age of 13. If a parent or guardian becomes aware that his or her child has provided us with information without their consent, he or she should contact us at kaluganin514@gmail.com. We will delete such information from our files within a reasonable time.
    <p><b>Security</b>
    <p>We are concerned about safeguarding the confidentiality of your information. We provide physical, electronic, and procedural safeguards to protect information we process and maintain.
    <p><b>Changes</b>
    <p>This Privacy Policy may be updated from time to time for any reason. We will notify you of any changes to our Privacy Policy by posting the new Privacy Policy here and informing you via email or text message. You are advised to consult this Privacy Policy regularly for any changes, as continued use is deemed approval of all changes.
    <p><b>Your Consent</b>
    <p>By using the Application, you are consenting to our processing of your information as set forth in this Privacy Policy now and as amended by us. "Processing,” means using cookies on a computer/hand held device or using or touching information in any way, including, but not limited to, collecting, storing, deleting, using, combining and disclosing information, all of which activities will take place in the United States. If you reside outside the United States your information will be transferred, processed and stored there under United States privacy standards.
    <p><b>Contact us</b>
    <p>If you have any questions regarding privacy while using the Application, or have questions about our practices, please contact us via email at kaluganin514@gmail.com.
</div>

<?php include('theme/footer.php'); ?>
<script src="/js/app.js"></script>
</body>
</html>