DROP DATABASE blog;
CREATE DATABASE blog;

CREATE TABLE  blog.articles (
  id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
  title TEXT NOT NULL ,
  title_en TEXT NOT NULL,
  article_date DATE NOT NULL ,
  content TEXT NOT NULL,
  content_en TEXT NOT NULL
);

CREATE TABLE  blog.users (
  id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
  username VARCHAR(255) NOT NULL ,
  email VARCHAR(255) NOT NULL ,
  password VARCHAR(255) NOT NULL
);

CREATE TABLE counters (
  id INT(1) UNSIGNED PRIMARY KEY,
  count INT(10) UNSIGNED NOT NULL,
  type VARCHAR(30) NOT NULL
);

INSERT INTO blog.articles (id, title, title_en, article_date, content, content_en) VALUES
  (1, 'Моя первая статья', 'My first article', '2011-09-01', '<p>Первая статья.</p>', '<p>THE FIRST ARTICLE</p>'),
  (2, 'Вторая статья', 'My second article', '2011-09-08', '<p>Вот и вторая статья</p>', '<p>Second one</p>'),
  (3, 'Третья статья', 'My third article', '2011-09-09', '<p>Третья статеющка</p>', '<p>The third article</p>'),
  (4, 'Четвертая статья', 'My fourth article', '2011-09-20', '<p>Да, это четвертая статья.</p>', '<p>Yes, it is the fourth article, YO!.</p>');
INSERT INTO blog.users (id, username, email, password) VALUE
  (1, 'TRTHHRTS', 'kaluganin514@gmail.com', '$2y$10$qlenetdar/FkhaHgPhpUPe89fgl1jaxf.dPPgwfasq73vGeo2Zcwe');
