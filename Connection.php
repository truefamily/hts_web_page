<?php

/**
 * Created by PhpStorm.
 * User: root
 * Date: 08.07.2016
 * Time: 23:30
 */
class Connection
{
    // Production:
    //const CONFIG_INI = '/home/u818119363/connect/config.ini';
    // Debug:
    const CONFIG_INI = '/config.ini';

    public static function getConfig() {
        return parse_ini_file(Connection::CONFIG_INI);
    }

    public static function getDb() {
        $conf = self::getConfig();
        $link = mysqli_connect($conf['mysql_host'],$conf['mysql_user'],$conf['mysql_password']);
        date_default_timezone_set('Europe/Moscow');
        /* проверяем соединение */
        if (mysqli_connect_errno()) {
            printf("Connect failed: %s\n", mysqli_connect_error());
            exit();
        }
        mysqli_select_db($link, $conf['mysql_database']) or die('Ошибка подключения к базе данных');

        return $link;
    }

    public static function getVersion() {
        $link = self::getDb();
        $result = mysqli_query($link, 'SELECT version FROM server_version LIMIT 1');
        mysqli_close($link);
        $version = $result->fetch_row();
        return $version[0];
    }

    public static function incrementDownloadsCounter() {
        $link = self::getDb();
        $result = mysqli_query($link, 'SELECT count FROM counters WHERE id = 1');
        $current = $result->fetch_field();
        echo $current;
        $current += 1;
        echo "<br/>" . $current;
        if (mysqli_query($link, "UPDATE counters SET count='$current' WHERE id = 1") === TRUE) {
            echo "Версия изменена.<br/>";
        } else {
            die(mysqli_error($link));
        }

        mysqli_close($link);
    }

    public static function getDownloadsCounter() {
        $link = self::getDb();
        $result = mysqli_query($link, 'SELECT count FROM counters WHERE id = 1');
        $current = $result->fetch_field();
        return $current;
    }


    // TODO Перенести сюда все запросы к БД
}