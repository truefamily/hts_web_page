<?php
include('theme/head.php');
include('theme/menu.php');
?>

<div class="container main_content">
    <p><b><?= $t['site.contacts.about'] ?></b></p>
    <table class="about_table">
        <tr>
            <td><b><?= $t['site.contacts.developed_by'] ?></b></td>
            <td><b><?= $t['site.contacts.contact_us'] ?></b></td>
        </tr>
        <tr>
            <td><?= $t['site.contacts.silakov'] ?></td>
            <td>
                <img src="/images/mail5.png" />
                <a href="mailto:kaluganin514@gmail.com">kaluganin514@gmail.com</a>
            </td>
        </tr>
        <tr>
            <td><?= $t['site.contacts.silakova'] ?></td>
            <td>
                <img src="/images/mail5.png" />
                <a href="mailto:dashasn2@gmail.com">dashasn2@gmail.com</a>
            </td>
        </tr>
    </table>
</div>


<script src="/js/app.js"></script>
</body>
</html>